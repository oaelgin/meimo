﻿using System;
using System.Collections.Generic;
using Meimo.Services.EmailNotifier.Core.Domain.Notification;

namespace Meimo.Services.EmailNotifier.DataAccess.Helpers
{
    public class EmailTemplateFieldComparer:IEqualityComparer<EmailTemplateField>
    {
        public bool Equals(EmailTemplateField x, EmailTemplateField y)
        {
            if (ReferenceEquals(x, y)) return true;
            if (ReferenceEquals(x, null)) return false;
            if (ReferenceEquals(y, null)) return false;
            if (x.GetType() != y.GetType()) return false;
            return x.Key == y.Key && x.Id == y.Id;
        }

        public int GetHashCode(EmailTemplateField obj)
        {
            return HashCode.Combine(obj.Key, obj.Template);
        }
    }
}