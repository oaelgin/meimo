﻿using Meimo.Services.EmailNotifier.Core.Domain.Notification;
using Microsoft.EntityFrameworkCore;

namespace Meimo.Services.EmailNotifier.DataAccess.Data
{
    public class EmailDataContext: DbContext
    {
        public DbSet<EmailTemplate> EmailTemplates { get; set; }
        public DbSet<EmailTemplateField> EmailTemplateFields { get; set; }
        
        public EmailDataContext()
        {
            
        }

        public EmailDataContext(DbContextOptions<EmailDataContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
        }
    }
}