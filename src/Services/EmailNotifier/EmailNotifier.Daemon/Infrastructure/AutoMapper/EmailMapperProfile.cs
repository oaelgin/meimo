﻿using AutoMapper;
using Meimo.Services.EmailNotifier.Infrastructure.Contracts;
using Meimo.Services.Infrastructure.Smtp.Model;

namespace Meimo.Services.EmailNotifier.Daemon.Infrastructure.AutoMapper
{
    public class EmailMapperProfile: Profile
    {
        public EmailMapperProfile()
        {
            CreateMap<RmqSendEmailMessage, SmtpSendEmailMessage>();
        }
    }
}