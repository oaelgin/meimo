﻿using System;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using AutoMapper;
using Meimo.Services.EmailNotifier.Core.Infrastructure.AutoMapper;
using Meimo.Services.EmailNotifier.Core.Infrastructure.Logging;
using Meimo.Services.EmailNotifier.Daemon.Infrastructure.AutoMapper;
using Meimo.Services.EmailNotifier.Daemon.Services;
using Meimo.Services.EmailNotifier.Infrastructure;
using Meimo.Services.EmailNotifier.Infrastructure.Consumers;
using Meimo.Services.Infrastructure.Smtp;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Sinks.Elasticsearch;
using ISendEmailService = Meimo.Services.EmailNotifier.Infrastructure.Consumers.ISendEmailService;

namespace Meimo.Services.EmailNotifier.Daemon
{
    class Program
    {
        private const string RabbitMqConfigSectionName = "RabbitMq";
        private const string SmtpConfigSectionName = "Smtp";
        
        static async Task Main(string[] args)
        {
            await new HostBuilder()
                .ConfigureServices((context, collection) =>
                {
                    IConfigurationRoot configuration = new ConfigurationBuilder()
                        .SetBasePath(Path.Combine(AppContext.BaseDirectory))
                        .AddJsonFile("appsettings.json", optional: false)
                        .AddEnvironmentVariables()
                        .Build();

                    var rabbitMqConfigSection = configuration.GetSection(RabbitMqConfigSectionName);
                    var smtpConfigSection = configuration.GetSection(SmtpConfigSectionName);

                    var serviceProvider = collection
                        .Configure<RabbitMqConfiguration>(rabbitMqConfigSection)
                        .Configure<SmtpClientConfiguration>(smtpConfigSection)
                        .AddAutoMapper(typeof(EmailMapperProfile))
                        .AddAutoMapper(typeof(DomainModelProfile))
                        .AddScoped<ITraceLogger, TraceLogger>()
                        .AddScoped<IEmailSender, EmailSender>()
                        .AddScoped<ISendEmailService, SendEmailService>()
                        .AddScoped<ISendEmailConsumer, SendEmailConsumer>()
                        .AddLogging(loggingBuilder =>
                        {
                            loggingBuilder.AddConsole();
                            loggingBuilder.AddSerilog();
                            loggingBuilder.AddDebug();
                        })
                        .AddHostedService<ConsumeRabbitMqHostedService>()
                        .BuildServiceProvider();

                    serviceProvider
                        .GetService<ILoggerFactory>()
                        .AddSerilog();

                    var environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
                    Log.Logger = new LoggerConfiguration()
                        .WriteTo.Console()
                        .WriteTo.Elasticsearch(ConfigureElasticSink(configuration, environment))
                        .CreateLogger();

                }).RunConsoleAsync();
        }
        
        private static ElasticsearchSinkOptions ConfigureElasticSink(IConfigurationRoot configuration, string environment)
        {
            return new ElasticsearchSinkOptions(new Uri(configuration["ElasticConfiguration:Uri"]))
            {
                AutoRegisterTemplate = true,
                IndexFormat = $"{Assembly.GetExecutingAssembly().GetName().Name.ToLower().Replace(".", "-")}-{environment?.ToLower().Replace(".", "-")}-{DateTime.UtcNow:yyyy-MM}"
            };
        }
    }
}