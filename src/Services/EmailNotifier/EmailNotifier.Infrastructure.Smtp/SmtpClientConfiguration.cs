﻿namespace Meimo.Services.Infrastructure.Smtp
{
    public class SmtpClientConfiguration
    {
        public int Port { get; set; }
        public string Host { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}