using Meimo.Person.AdditionalInfo.DataAccess.Data;
using Meimo.Person.AdditionalInfo.DataAccess.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Meimo.Person.AdditionalInfo.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // requires using Microsoft.Extensions.Options
            services.Configure<AdditionalInfoDatabaseSettings>(
                Configuration.GetSection(nameof(AdditionalInfoDatabaseSettings)));

            services.AddSingleton(sp =>
                sp.GetRequiredService<IOptions<AdditionalInfoDatabaseSettings>>().Value);

            services.AddSingleton<AdditionalInfoService>();

            services.AddControllers()
                .AddMvcOptions(x => x.SuppressAsyncSuffixInActionNames = false);

            services.AddOpenApiDocument(options =>
            {
                options.Title = "Meimo AdditionalInfo Factory API Doc";
                options.Version = "1.0";
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseOpenApi();
            app.UseSwaggerUi3(x =>
            {
                x.DocExpansion = "list";
            });

            //app.UseHttpsRedirection();

            app.UseRouting();

            //app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
