﻿using System.Collections.Generic;

namespace Meimo.Person.Account.WebApi.Errors
{
    public class ApiResponseExtended : ApiResponse
    {
        public ApiResponseExtended() : base(400)
        {

        }

        public ApiResponseExtended(int statusCode, IEnumerable<ModelErrorKeyValues> errors = null)
            : base(statusCode)
        {
            Errors = errors;
        }

        public IEnumerable<ModelErrorKeyValues> Errors { get; set; }
    }
}
