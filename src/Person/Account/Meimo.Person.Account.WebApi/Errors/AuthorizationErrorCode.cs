﻿using System.Runtime.Serialization;

namespace Meimo.Person.Account.WebApi.Errors
{
    [DataContract]
    public enum AuthorizationErrorCode
    {
        [EnumMember(Value = "Неизвестная ошибка")]
        Unknown = 0,

        [EnumMember(Value = "Аккаунт не активирован")]
        NotActivated = 1,

        [EnumMember(Value = "Аккаунт заблокирован")]
        Locked = 2,

        [EnumMember(Value = "Аккаунт помечен как удаленный")]
        Deleted = 3
    }
}
