﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace Meimo.Person.Account.EmailNotifierClient
{
    public class EmailSender : IEmailSender
    {
        private readonly HttpClient _httpClient;
        private readonly EmailOptions _emailOptions;
        private readonly bool _hasEmailTemplate;
        private readonly Guid _templateId;
        private readonly JsonSerializerOptions _jsonSerializerOptions;

        public EmailSender(HttpClient client, EmailOptions emailOptions)
        {
            _httpClient = client;
            _emailOptions = emailOptions;
            _hasEmailTemplate = Guid.TryParse(_emailOptions.TemplateId, out _templateId)
                                && _templateId != Guid.Empty;

            _jsonSerializerOptions = new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                IgnoreNullValues = true,
                WriteIndented = true
            };
        }

        public async Task<bool> SendEmailAsync(string toAddress, string subject, string body)
        {
            if (string.IsNullOrWhiteSpace(toAddress))
            {
                throw new ArgumentNullException(nameof(toAddress));
            }

            if (_hasEmailTemplate)
            {
                var templateRequest = new SendEmailTemplateRequest()
                {
                    From = _emailOptions.FromAddress,
                    To = toAddress,
                    Subject = subject,
                    EmailTemplateId = _templateId,
                    EmailTemplateParameters = new List<SendingEmailTemplateParameterRequest>()
                    {
                        new SendingEmailTemplateParameterRequest()
                        {
                            Key = "ActivationURL",
                            Value = body
                        }
                    }
                };

                return await SendEmailTemplateAsync(templateRequest);
            }

            var simpleRequest = new SendEmailRequest()
            {
                From = _emailOptions.FromAddress,
                To = toAddress,
                Subject = subject,
                Body = body
            };

            return await SendEmailAsync(simpleRequest);
        }

        public async Task<bool> SendEmailAsync(SendEmailRequest request)
        {
            return await SendEmailAsync(request, CancellationToken.None);
        }

        public async Task<bool> SendEmailAsync(
            SendEmailRequest request, CancellationToken cancellationToken)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            var content = new StringContent(
                JsonSerializer.Serialize(request, _jsonSerializerOptions),
                Encoding.UTF8,
                "application/json");

            using var httpResponse = await _httpClient
                .PostAsync("/api/v1/Send/SendEmail", content, cancellationToken);

            return httpResponse.IsSuccessStatusCode;
        }

        public async Task<bool> SendEmailTemplateAsync(SendEmailTemplateRequest request)
        {
            return await SendEmailTemplateAsync(request, CancellationToken.None);
        }

        public async Task<bool> SendEmailTemplateAsync(
            SendEmailTemplateRequest request, CancellationToken cancellationToken)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            var content = new StringContent(
                JsonSerializer.Serialize(request, _jsonSerializerOptions),
                Encoding.UTF8,
                "application/json");

            using var httpResponse = await _httpClient
                .PostAsync("/api/v1/Send/SendEmailTemplate", content, cancellationToken);

            return httpResponse.IsSuccessStatusCode;
        }
    }
}
