﻿using Microsoft.Extensions.Configuration;

namespace Meimo.Person.Account.PostgreSQL.EF.DbInit
{
    public static class ConnectionStringFactory
    {
        public const string DbConnectionName = @"DefaultConnection";
        public static string GetDbConnectionString()
        {
            IConfiguration config = new ConfigurationBuilder()
                .AddUserSecrets<Program>()
                .Build();

            return config.GetConnectionString(DbConnectionName);
        }
    }
}
