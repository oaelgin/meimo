﻿namespace Meimo.Person.Account.Core.Dtos
{
    public class ProfileShortInfoDto
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Avatar { get; set; }

        public bool Locked { get; set; }

        public bool Deleted { get; set; }
    }
}
