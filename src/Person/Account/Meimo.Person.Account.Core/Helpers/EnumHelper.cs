﻿using System;
using System.Reflection;
using System.Runtime.Serialization;

namespace Meimo.Person.Account.Core.Helpers
{
    public static class EnumHelper
    {
        public static string GetEnumMember(this Enum value)
        {
            FieldInfo fieldInfo = value.GetType().GetField(value.ToString());

            EnumMemberAttribute enumMemberAttribute = (EnumMemberAttribute)fieldInfo
                .GetCustomAttribute(typeof(EnumMemberAttribute), false);

            if (enumMemberAttribute == null || !enumMemberAttribute.IsValueSetExplicitly)
            {
                return value.ToString();
            }

            return enumMemberAttribute.Value;
        }
    }
}
