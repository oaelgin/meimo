﻿using System;
using System.Collections.Generic;
using System.Text;

using Meimo.Person.Account.Core.Interfaces;

namespace Meimo.Person.Account.Core.Helpers
{
    public class GuidNew : IGuidNew
    {
        public Guid Value { get; }

        public GuidNew() : this(null) { }

        public GuidNew(Guid? value)
        {
            if (value != null && value.HasValue)
            {
                Value = value.Value;
                return;
            }

            Value = Guid.NewGuid();
        }
    }
}
