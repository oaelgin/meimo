﻿using System;

using Meimo.Person.Account.Core.Interfaces;

namespace Meimo.Person.Account.Core.Helpers
{
    public class DateTimeOffsetNow : IDateTimeOffsetNow
    {
        public DateTimeOffset Value { get; }

        public DateTimeOffsetNow(): this(null) { }

        public DateTimeOffsetNow(DateTimeOffset? value)
        {
            if (value != null && value.HasValue)
            {
                Value = value.Value;
                return;
            }

            Value = DateTimeOffset.Now;
        }
    }
}
