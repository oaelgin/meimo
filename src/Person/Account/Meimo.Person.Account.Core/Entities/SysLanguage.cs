﻿namespace Meimo.Person.Account.Core.Entities
{
    public enum SysLanguage : short
    {
        Russian = 1,
        English = 2
    }
}
