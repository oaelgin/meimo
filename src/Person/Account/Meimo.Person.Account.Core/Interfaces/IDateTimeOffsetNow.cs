﻿using System;

namespace Meimo.Person.Account.Core.Interfaces
{
    public interface IDateTimeOffsetNow
    {
        DateTimeOffset Value { get; }
    }
}
