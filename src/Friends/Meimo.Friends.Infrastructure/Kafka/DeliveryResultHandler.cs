using System;
using Confluent.Kafka;
using Meimo.Friends.Kafka.Contracts;
using Microsoft.Extensions.Logging;

namespace Meimo.Friends.Infrastructure.Kafka
{
    public static class DeliveryResultHandler
    {
        public static ILogger<KafkaProducer>? Logger { get; set; }
        
        public static void Handler(DeliveryReport<Null, NotificationEvent> r)
        {
            if (!r.Error.IsError)
            {
                Logger.LogInformation(
                    $"Delivered {r.Value}, id = {r.Value.Id}\n"+
                    $"        to topic {r.Topic} partition {r.Partition} offset {r.Offset}");
            }
            else
            {
                Logger.LogWarning(
                    $"Delivery failed: {r.Error.Reason}\n" +
                    $"DestinationId: {r.Value.DestinationId}, Message: {r.Message}");
            }
        }
    }
}