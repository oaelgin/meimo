using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Meimo.Friends.Core;
using Meimo.Friends.Infrastructure.Kafka;
using Meimo.Friends.Kafka.Contracts;
using Meimo.Friends.ProfileGrpcClient;
using Microsoft.Extensions.Logging;
using Neo4j.Driver;

namespace Meimo.Friends.Infrastructure
{
    public class RelationshipRepository : IRelationshipRepository
    {
        private const string Id1ParameterName = "id1";
        private const string Guid1ParameterName = "guid1";
        private const string P1FirstNameResultName = "p1FirstName";
        private const string Id2ParameterName = "id2";
        private const string Guid2ParameterName = "guid2";
        private const string P2FirstNameResultName = "p2FirstName";

        private const string P1NodeSelectorStatement =
            "(p1:Person{id: $" + Id1ParameterName + ", guid: $" + Guid1ParameterName + "})";

        private const string P2NodeSelectorStatement =
            "(p2:Person{id: $" + Id2ParameterName + ", guid: $" + Guid2ParameterName + "})";
        
        private const string MergeStatementSection =
            "MERGE " + P1NodeSelectorStatement + "\n" +
            "MERGE " + P2NodeSelectorStatement + "\n";
            
        private const string ReturnFirstNamesStatementSection =
            "RETURN p1.firstName as " + P1FirstNameResultName + ", p2.firstName as " + P2FirstNameResultName;
            
        private const string MakeFriendStatementTemplate = 
            MergeStatementSection +
            "WITH p1, p2\n" +
            "WHERE NOT EXISTS { (p1)-[:REQUEST]-(p2) }\n" +
            "  AND NOT EXISTS { (p1)-[:SUBSCRIBED_TO]->(p2) }\n" +
            "  AND NOT EXISTS { (p1)-[:FRIEND]-(p2) }\n" +
            "  AND NOT EXISTS { (p1)-[:HAS_BLOCKED]-(p2) }\n" +
            "CREATE (p1)-[:REQUEST]->(p2)\n" +
            "SET p1.numberOfOutgoingRequests = coalesce(p1.numberOfOutgoingRequests, 0) + 1,\n" +
            "    p2.numberOfIncomingRequests = coalesce(p2.numberOfIncomingRequests, 0) + 1\n" +
            ReturnFirstNamesStatementSection;

        private const string AcceptFriendStatementTemplate =
            "MATCH " + P1NodeSelectorStatement + "<-[request:REQUEST]-" + P2NodeSelectorStatement + "\n" +
            "CREATE (p1)<-[:FRIEND]-(p2)\n" +
            "DELETE request\n" +
            "SET p1.numberOfIncomingRequests = p1.numberOfIncomingRequests - 1,\n" +
            "    p2.numberOfOutgoingRequests = p2.numberOfOutgoingRequests - 1,\n" +
            "    p1.numberOfFriends = coalesce(p1.numberOfFriends, 0) + 1,\n" +
            "    p2.numberOfFriends = coalesce(p2.numberOfFriends, 0) + 1\n" +
            "WITH p1, p2\n" +
            "MATCH (p1)-[subscribed_to:SUBSCRIBED_TO]->(p2)\n" +
            "DELETE subscribed_to\n" +
            "SET p1.numberOfOutgoingSubscriptions = p1.numberOfOutgoingSubscriptions - 1,\n" +
            "    p2.numberOfIncomingSubscriptions = p2.numberOfIncomingSubscriptions - 1";


        private const string LeaveInSubscribersStatementTemplate =
            "MATCH " + P1NodeSelectorStatement + "\n" +
            "MATCH " + P2NodeSelectorStatement + "\n" +
            "OPTIONAL MATCH (p1)<-[request:REQUEST]-(p2)\n" +
            "OPTIONAL MATCH (p1)-[friend:FRIEND]-(p2)\n" +
            "WITH p1, p2, request, friend\n" +
            "WHERE request is not null OR friend is NOT NULL\n" +
            "SET (CASE WHEN request is not null THEN p1 END).numberOfIncomingRequests = p1.numberOfIncomingRequests - 1,\n" +
            "    (CASE WHEN request is not null THEN p2 END).numberOfOutgoingRequests = p2.numberOfOutgoingRequests - 1,\n" +
            "    (CASE WHEN friend is not null THEN p1 END).numberOfFriends = p1.numberOfFriends - 1,\n" +
            "    (CASE WHEN friend is not null THEN p2 END).numberOfFriends = p2.numberOfFriends - 1,\n" +
            "    p1.numberOfIncomingSubscriptions = coalesce(p1.numberOfIncomingSubscriptions, 0) + 1,\n" +
            "    p2.numberOfOutgoingSubscriptions = coalesce(p2.numberOfOutgoingSubscriptions, 0) + 1\n" +
            "CREATE (p1)<-[:SUBSCRIBED_TO]-(p2)\n" +
            "DELETE request\n" +
            "DELETE friend";

        private const string SubscribeStatementTemplate =
            MergeStatementSection +
            "WITH p1, p2\n" +
            "WHERE NOT EXISTS { (p1)-[:REQUEST]-(p2) }\n" +
              "AND NOT EXISTS { (p1)-[:SUBSCRIBED_TO]->(p2) }\n" +
              "AND NOT EXISTS { (p1)-[:FRIEND]-(p2) }\n" +
              "AND NOT EXISTS { (p1)-[:HAS_BLOCKED]-(p2) }\n" +
            "CREATE (p1)-[:SUBSCRIBED_TO]->(p2)\n" +
            "SET p1.numberOfOutgoingSubscriptions = coalesce(p1.numberOfOutgoingSubscriptions, 0) + 1,\n" +
            "    p2.numberOfIncomingSubscriptions = coalesce(p2.numberOfIncomingSubscriptions, 0) + 1\n" +
            ReturnFirstNamesStatementSection;

        private const string DeleteSubscriptionStatementTemplate =
            "MATCH " + P1NodeSelectorStatement + "-[subscribed_to:SUBSCRIBED_TO]->" + P2NodeSelectorStatement + "\n" +
            "DELETE subscribed_to\n" +
            "SET p1.numberOfOutgoingSubscriptions = p1.numberOfOutgoingSubscriptions - 1,\n" +
            "    p2.numberOfIncomingSubscriptions = p2.numberOfIncomingSubscriptions - 1";

        private const string AddToBlacklistStatementTemplate =
            MergeStatementSection +
            "WITH p1, p2\n" +
            "WHERE NOT EXISTS { (p1)-[:HAS_BLOCKED]->(p2) }\n" +
            "OPTIONAL MATCH (p1)-[req_out:REQUEST]->(p2)\n" +
            "OPTIONAL MATCH (p1)<-[req_in:REQUEST]-(p2)\n" +
            "OPTIONAL MATCH (p1)-[sub_out:SUBSCRIBED_TO]->(p2)\n" +
            "OPTIONAL MATCH (p1)<-[sub_in:SUBSCRIBED_TO]-(p2)\n" +
            "OPTIONAL MATCH (p1)-[friend:FRIEND]-(p2)\n" +
            "WITH p1, p2, req_out, req_in, sub_out, sub_in, friend\n" +
            "SET (CASE WHEN req_out is not null THEN p2 END).numberOfIncomingRequests = p2.numberOfIncomingRequests - 1,\n" +
            "    (CASE WHEN req_out is not null THEN p1 END).numberOfOutgoingRequests = p1.numberOfOutgoingRequests - 1,\n" +
            "    (CASE WHEN req_in is not null THEN p1 END).numberOfIncomingRequests = p1.numberOfIncomingRequests - 1,\n" +
            "    (CASE WHEN req_in is not null THEN p2 END).numberOfOutgoingRequests = p2.numberOfOutgoingRequests - 1,\n" +
            "    (CASE WHEN sub_out is not null THEN p2 END).numberOfIncomingSubscriptions = p2.numberOfIncomingSubscriptions - 1,\n" +
            "    (CASE WHEN sub_out is not null THEN p1 END).numberOfOutgoingSubscriptions = p1.numberOfOutgoingSubscriptions - 1,\n" +
            "    (CASE WHEN sub_in is not null THEN p1 END).numberOfIncomingSubscriptions = p1.numberOfIncomingSubscriptions - 1,\n" +
            "    (CASE WHEN sub_in is not null THEN p2 END).numberOfOutgoingSubscriptions = p2.numberOfOutgoingSubscriptions - 1,\n" +
            "    (CASE WHEN friend is not null THEN p1 END).numberOfFriends = p1.numberOfFriends - 1,\n" +
            "    (CASE WHEN friend is not null THEN p2 END).numberOfFriends = p2.numberOfFriends - 1,\n" +
            "    p1.numberOfOutgoingLocks = coalesce(p1.numberOfOutgoingLocks, 0) + 1,\n" +
            "    p2.numberOfIncomingLocks = coalesce(p2.numberOfIncomingLocks, 0) + 1\n" +
            "CREATE (p1)-[:HAS_BLOCKED]->(p2)\n" +
            "DELETE req_out, req_in, sub_out, sub_in, friend\n" +
            ReturnFirstNamesStatementSection;
        
        private const string RemoveFromBlacklistStatementTemplate =
            "MATCH " + P1NodeSelectorStatement + "-[hb:HAS_BLOCKED]->" + P2NodeSelectorStatement + "\n" +
            "DELETE hb\n" +
            "SET p1.numberOfOutgoingLocks = p1.numberOfOutgoingLocks - 1,\n" +
            "    p2.numberOfIncomingLocks = p2.numberOfIncomingLocks - 1";

        private const string IsBlockedByStatementTemplate =
            "MATCH " + P1NodeSelectorStatement + "\n" +
            "MATCH " + P2NodeSelectorStatement + "\n" +
            "OPTIONAL MATCH (p1)<-[blockedBy:HAS_BLOCKED]-(p2)\n" +
            "RETURN (blockedBy is not null) as isBlockedBy";

        private readonly IDriver _driver;
        private readonly IFriendsDbSettings _friendsDbSettings;
        private readonly Profiles.ProfilesClient _profilesClient;
        private readonly ILogger<RelationshipRepository> _logger;
        private readonly IPersonRepository _personRepository;
        private readonly KafkaProducer _producer;
        private readonly FriendsEventMessageSerializer _friendsEventMessageSerializer;

        public RelationshipRepository(
            IDriver driver, 
            IFriendsDbSettings friendsDbSettings,
            Profiles.ProfilesClient profilesClient,
            ILogger<RelationshipRepository> logger,
            IPersonRepository personRepository,
            KafkaProducer producer,
            FriendsEventMessageSerializer friendsEventMessageSerializer)
        {
            _driver = driver;
            _friendsDbSettings = friendsDbSettings;
            _profilesClient = profilesClient;
            _logger = logger;
            _personRepository = personRepository;
            _producer = producer;
            _friendsEventMessageSerializer = friendsEventMessageSerializer;
        }

        private static Dictionary<string, object> CreatePersonStatementParameters(Guid person1, Guid person2)
        {
            return new Dictionary<string, object>
            {
                { Id1ParameterName, person1.ToString() },
                { Guid1ParameterName, person1.ToByteArray() },
                { Id2ParameterName, person2.ToString() },
                { Guid2ParameterName, person2.ToByteArray() }
            };
        }
        
        private IAsyncSession GetAsyncSession(AccessMode defaultAccessMode)
        {
            return _driver.AsyncSession(builder => builder
                .WithDatabase(_friendsDbSettings.DatabaseName)
                .WithDefaultAccessMode(defaultAccessMode));
        }

        private async Task SetProfileInfoToNewNodesAsync(MergeResult mergeResult)
        {
            if (mergeResult.ModifyResult!.NodesCreated == 0) return;
            
            var personIds = new List<Guid>();
            
            if (!mergeResult.Person1HasFirstName)
            {
                personIds.Add(mergeResult.Person1Id);
            }
            
            if (!mergeResult.Person2HasFirstName)
            {
                personIds.Add(mergeResult.Person2Id);
            }

            foreach (var id in personIds)
            {
                try
                {
                    var profileMiddleInfo = await _profilesClient.GetProfileMiddleInfoAsync(
                        new ProfileLookupModel{ Id = id.ToString() });

                    var succeeded = await _personRepository.SetPersonProfileInfo(new Person
                    {
                        Id = Guid.Parse(profileMiddleInfo.Id),
                        FirstName = profileMiddleInfo.FirstName,
                        LastName = profileMiddleInfo.LastName,
                        Avatar = profileMiddleInfo.Avatar,
                        City = profileMiddleInfo.City,
                        Country = profileMiddleInfo.Country,
                        Locked = profileMiddleInfo.Locked,
                        Deleted = profileMiddleInfo.Deleted
                    });

                    if (!succeeded)
                    {
                        _logger.LogWarning(
                            $"Неизвестная ошибка при изменении данных пользователя {profileMiddleInfo.Id}");
                    }
                }
                catch (Exception e)
                {
                    //ToDo отправка сообщения в очередь на отложенное получение данных через очередь
                    _logger.LogWarning(e, "Ошибка при получении и изменении данных профиля");
                }
            }
        }
        
        private async Task<MergeResult> RunWithMergePersonsAsync(
            Guid person1,
            Guid person2, 
            string statementTemplate, 
            IDictionary<string, object> statementParameters)
        {
            var session = GetAsyncSession(AccessMode.Write);
            
            try
            {
                var cursor = await session.RunAsync(statementTemplate, statementParameters);
                var mergeResult = new MergeResult
                {
                    Person1Id = person1,
                    Person2Id = person2
                };
                
                if (await cursor.FetchAsync())
                {
                    mergeResult.SetHasFirstNameProperties(
                        !string.IsNullOrWhiteSpace(cursor.Current[P1FirstNameResultName].As<string>()),
                        !string.IsNullOrWhiteSpace(cursor.Current[P2FirstNameResultName].As<string>()));
                }

                mergeResult.ModifyResult = (await cursor.ConsumeAsync()).Counters.ToGraphModifyResult();
                await  SetProfileInfoToNewNodesAsync(mergeResult);
                return mergeResult;
            }
            finally
            {
                await session.CloseAsync();
            }
        }

        private async Task<GraphModifyResult> RunWriteAsync(
            string statementTemplate, 
            IDictionary<string, object> statementParameters)
        {
            var session = GetAsyncSession(AccessMode.Write);
            
            try
            {
                var cursor = await session.RunAsync(statementTemplate, statementParameters);
                return (await cursor.ConsumeAsync()).Counters.ToGraphModifyResult();
            }
            finally
            {
                await session.CloseAsync();
            }
        }
        
        public async Task<MergeResult> MakeFriendRequest(Guid person1, Guid person2)
        {
            var statementParameters = CreatePersonStatementParameters(person1, person2);
            
            var result = await RunWithMergePersonsAsync(
                person1, person2, MakeFriendStatementTemplate, statementParameters);

            if (result.ModifyResult!.RelationshipsCreated == 1)
            {
                _producer.Produce(new NotificationEvent
                {
                    Id = Guid.NewGuid(),
                    DestinationId = person2,
                    InitiatorId = person1,
                    EventType = NotificationEventType.Friends,
                    Message = _friendsEventMessageSerializer.Serialize(new FriendsEventMessage
                    {
                        PersonId = person1,
                        MessageType = FriendsEventMessageType.Request,
                        MessageTemplate = "Пользователь {0} подал заявку на добавление в друзья"
                    }),
                    EventChannels = new List<NotificationEventChannel>
                    {
                        NotificationEventChannel.BrowserNotificationFeed,
                        NotificationEventChannel.BrowserToast,
                        NotificationEventChannel.BrowserDesktop,
                        NotificationEventChannel.Email
                    }
                });
            }
            
            return result;
        }

        public async Task<GraphModifyResult> AcceptFriendRequest(Guid person1, Guid person2)
        {
            var statementParameters = CreatePersonStatementParameters(person1, person2);
            
            var result = await RunWriteAsync(AcceptFriendStatementTemplate, statementParameters);

            if (result.RelationshipsCreated == 1)
            {
                _producer.Produce(new NotificationEvent
                {
                    Id = Guid.NewGuid(),
                    DestinationId = person2,
                    InitiatorId = person1,
                    EventType = NotificationEventType.Friends,
                    Message = _friendsEventMessageSerializer.Serialize(new FriendsEventMessage
                    {
                        PersonId = person1,
                        MessageType = FriendsEventMessageType.Accept,
                        MessageTemplate = "Пользователь {0} принял Вашу заявку на добавление в друзья"
                    }),
                    EventChannels = new List<NotificationEventChannel>
                    {
                        NotificationEventChannel.BrowserNotificationFeed,
                        NotificationEventChannel.BrowserToast,
                        NotificationEventChannel.BrowserDesktop,
                        NotificationEventChannel.Email
                    }
                });
            }
            
            return result;
        }

        public async Task<GraphModifyResult> LeaveInSubscribers(Guid person1, Guid person2)
        {
            var statementParameters = CreatePersonStatementParameters(person1, person2);
            return await RunWriteAsync(LeaveInSubscribersStatementTemplate, statementParameters);
        }

        public async Task<MergeResult> Subscribe(Guid person1, Guid person2)
        {
            var statementParameters = CreatePersonStatementParameters(person1, person2);
            return await RunWithMergePersonsAsync(
                person1, person2, SubscribeStatementTemplate, statementParameters);
        }

        private async Task<GraphModifyResult> DeleteSubscription(Guid personIdFrom, Guid personIdTo)
        {
            var statementParameters = CreatePersonStatementParameters(personIdFrom, personIdTo);
            return await RunWriteAsync(DeleteSubscriptionStatementTemplate, statementParameters);
        }
        
        public async Task<GraphModifyResult> Unsubscribe(Guid person1, Guid person2)
        {
            return await DeleteSubscription(person1, person2);
        }

        public async Task<GraphModifyResult> RemoveFromSubscribers(Guid person1, Guid person2)
        {
            return await DeleteSubscription(person2, person1);
        }

        public async Task<MergeResult> AddToBlacklist(Guid person1, Guid person2)
        {
            var statementParameters = CreatePersonStatementParameters(person1, person2);
            return await RunWithMergePersonsAsync(
                person1, person2, AddToBlacklistStatementTemplate, statementParameters);
        }

        public async Task<GraphModifyResult> RemoveFromBlacklist(Guid person1, Guid person2)
        {
            var statementParameters = CreatePersonStatementParameters(person1, person2);
            return await RunWriteAsync(RemoveFromBlacklistStatementTemplate, statementParameters);
        }

        public async Task<IList<Relationship>> GetRelationships(Guid person1, Guid person2)
        {
            var statementParameters = CreatePersonStatementParameters(person1, person2);
            
            var getRelationsStatementTemplate =
                "MATCH " + P1NodeSelectorStatement + "\n" +
                "MATCH " + P2NodeSelectorStatement + "\n" +
                "OPTIONAL MATCH (p1)-[friend:FRIEND]-(p2)\n" +
                "OPTIONAL MATCH (p1)-[requestTo:REQUEST]->(p2)\n" +
                "OPTIONAL MATCH (p1)<-[requestFrom:REQUEST]-(p2)\n" +
                "OPTIONAL MATCH (p1)-[subscriptionTo:SUBSCRIBED_TO]->(p2)\n" +
                "OPTIONAL MATCH (p1)<-[subscriptionFrom:SUBSCRIBED_TO]-(p2)\n" +
                "OPTIONAL MATCH (p1)-[hasBlocked:HAS_BLOCKED]->(p2)\n" +
                "OPTIONAL MATCH (p1)<-[blockedBy:HAS_BLOCKED]-(p2)\n" +
                "RETURN [ x IN [\n" +
                "    CASE WHEN friend is not null THEN " + RelationType.Friend.ToString("D") + " END,\n" +
                "    CASE WHEN requestFrom is not null THEN " + RelationType.RequestFrom.ToString("D") + " END,\n" +
                "    CASE WHEN requestTo is not null THEN " + RelationType.RequestTo.ToString("D") + " END,\n" +
                "    CASE WHEN subscriptionFrom is not null THEN " + RelationType.SubscriptionFrom.ToString("D") + " END,\n" +
                "    CASE WHEN subscriptionTo is not null THEN " + RelationType.SubscriptionTo.ToString("D") + " END,\n" +
                "    CASE WHEN blockedBy is not null THEN " + RelationType.BlockedBy.ToString("D") + " END,\n" +
                "    CASE WHEN hasBlocked is not null THEN " + RelationType.HasBlocked.ToString("D") + " END\n" +
                "  ] WHERE x is not null] as relations";

            var relationships = new List<Relationship>();
            var session = GetAsyncSession(AccessMode.Read);

            try
            {
                var cursor = await session.RunAsync(getRelationsStatementTemplate, statementParameters);

                if (!await cursor.FetchAsync()) return relationships;

                var queryResult = cursor.Current["relations"].As<List<int>>();
                queryResult.ForEach(x => relationships.Add(new Relationship
                {
                    FirstPersonId = person1,
                    SecondPersonId = person2,
                    Type = (RelationType)x 
                }));
                return relationships;
            }
            finally
            {
                await session.CloseAsync();
            }
        }

        public async Task<bool> GetIsBlockedBy(Guid person1, Guid person2)
        {
            var statementParameters = CreatePersonStatementParameters(person1, person2);
            var session = GetAsyncSession(AccessMode.Read);
            try
            {
                var cursor = await session.RunAsync(IsBlockedByStatementTemplate, statementParameters);
                
                if (!await cursor.FetchAsync()) return false;
                
                var queryResult = cursor.Current["isBlockedBy"].As<bool>();
                return queryResult;
            }
            finally
            {
                await session.CloseAsync();
            }
        }
    }
}
