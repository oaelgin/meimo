using Meimo.Friends.Core;
using Neo4j.Driver;

namespace Meimo.Friends.Infrastructure
{
    public static class ResultSummaryExtensions
    {
        public static GraphModifyResult ToGraphModifyResult(this ICounters counters)
        {
            return new GraphModifyResult
            {
                NodesCreated = counters.NodesCreated,
                NodesDeleted = counters.NodesDeleted,
                RelationshipsCreated = counters.RelationshipsCreated,
                RelationshipsDeleted = counters.RelationshipsDeleted
            };
        }
    }
}
