namespace Meimo.Friends.Kafka.Contracts
{
    public enum NotificationEventType
    {
        Unknown = 0,
        Friends = 1,
        Club = 2
    }
}