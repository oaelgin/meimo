using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Meimo.Friends.Kafka.Contracts
{
    public class NotificationEvent
    {
        /// <summary>
        /// Флаг выставляемый в true при десериализации,
        /// если из очереди получено пустое Value
        /// </summary>
        [JsonIgnore]
        public bool IsNull { get; set; }

        /// <summary>
        /// Флаг выставляемый в true при десериализации,
        /// если получен пустой или неполный объект
        /// </summary>
        [JsonIgnore]
        public bool IsEmpty { get; set; }
        
        /// <summary>
        /// Уникальный идентификатор события
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Идентификатор пользователя которому предназначено уведомление
        /// </summary>
        public Guid DestinationId { get; set; }

        /// <summary>
        /// Идентификатор пользователя инициировавшего событие
        /// </summary>
        public Guid? InitiatorId { get; set; }
        
        /// <summary>
        /// Тип или сервис инициировавший событие
        /// </summary>
        public NotificationEventType EventType { get; set; }

        /// <summary>
        /// Сообщение
        /// </summary>
        public string? Message { get; set; }

        /// <summary>
        /// Список каналов по которым требуется отправить уведомление 
        /// </summary>
        public IList<NotificationEventChannel>? EventChannels { get; set; }
    }
}