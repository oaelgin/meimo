using System;
using System.Threading.Tasks;
using HotChocolate;
using HotChocolate.Types;
using Meimo.Friends.Core;
using Meimo.Friends.GraphQL.Models;

namespace Meimo.Friends.GraphQL.TypeExtensions
{
    [ExtendObjectType(Constants.MutationTypeName)]
    public class RelationshipMutations
    {
        private static UserError? RelationshipInputValidate(RelationshipInput input)
        {
            if (input.FirstPersonId == input.SecondPersonId)
            {
                return new UserError("Идентификаторы пользователей должны быть разными",
                    "ARGUMENT_ERROR");
            }

            return null;
        }

        private static UserError? CheckCommonResult(
            int relationshipsCreated, int relationshipsDeleted, string code)
        {
            return relationshipsCreated switch
            {
                0 when relationshipsDeleted == 0 => new UserError(
                    "Существующие связи между пользователями не позволяют выполнить операцию", code),
                > 1 => new UserError("При изменении связей между пользователями произошла неизвестная ошибка", code),
                _ => null
            };
        }

        private async Task<RelationshipPayload> RunChangeRelationshipsWithNodeMerge(
            Func<Guid, Guid, Task<MergeResult>> changeFunction,
            RelationshipInput input, RelationType relationType, string errorCode)
        {
            var validationError = RelationshipInputValidate(input);
            if (validationError is not null) return new RelationshipPayload(validationError); 

            var result = await changeFunction(input.FirstPersonId, input.SecondPersonId);

            var resultCommonError = CheckCommonResult(
                result.ModifyResult!.RelationshipsCreated, 
                result.ModifyResult!.RelationshipsCreated,
                errorCode); 
            if (resultCommonError is not null) return new RelationshipPayload(resultCommonError);
            
            return new RelationshipPayload(new Relationship
            {
                FirstPersonId = input.FirstPersonId,
                SecondPersonId = input.SecondPersonId,
                Type = relationType
            });
        }

        private async Task<RelationshipPayload> RunChangeRelationships(
            Func<Guid, Guid, Task<GraphModifyResult>> changeFunction,
            RelationshipInput input, RelationType? relationType, string errorCode)
        {
            var validationError = RelationshipInputValidate(input);
            if (validationError is not null) return new RelationshipPayload(validationError); 

            var result = await changeFunction(input.FirstPersonId, input.SecondPersonId);
            
            var resultCommonError = CheckCommonResult(
                result.RelationshipsCreated,
                result.RelationshipsDeleted,
                errorCode);
            if (resultCommonError is not null) return new RelationshipPayload(resultCommonError);

            return new RelationshipPayload(new Relationship
            {
                FirstPersonId = input.FirstPersonId,
                SecondPersonId = input.SecondPersonId,
                Type = relationType
            });
        }
        
        public async Task<RelationshipPayload> MakeFriendRequest(
            RelationshipInput input, [Service] IRelationshipRepository repository)
        {
            return await RunChangeRelationshipsWithNodeMerge(
                repository.MakeFriendRequest,
                input,
                RelationType.RequestTo,
                "MAKE_FRIEND_REQUEST_FAILED");
        }

        public async Task<RelationshipPayload> AcceptFriendRequest(
            RelationshipInput input, [Service] IRelationshipRepository repository)
        {
            return await RunChangeRelationships(
                repository.AcceptFriendRequest,
                input,
                RelationType.Friend,
                "ACCEPT_FRIEND_REQUEST_FAILED");            
        }

        public async Task<RelationshipPayload> LeaveInSubscribers(
            RelationshipInput input, [Service] IRelationshipRepository repository)
        {
            return await RunChangeRelationships(
                repository.LeaveInSubscribers,
                input,
                RelationType.SubscriptionFrom,
                "LEAVE_IN_SUBSCRIBERS_FAILED");            
        }
        
        public async Task<RelationshipPayload> Subscribe(
            RelationshipInput input, [Service] IRelationshipRepository repository)
        {
            return await RunChangeRelationshipsWithNodeMerge(
                repository.Subscribe,
                input,
                RelationType.SubscriptionTo,
                "SUBSCRIBE_FAILED");            
        }
        
        public async Task<RelationshipPayload> Unsubscribe(
            RelationshipInput input, [Service] IRelationshipRepository repository)
        {
            return await RunChangeRelationships(
                repository.Unsubscribe,
                input,
                null,
                "UNSUBSCRIBE_FAILED");            
        }

        public async Task<RelationshipPayload> RemoveFromSubscribers(
            RelationshipInput input, [Service] IRelationshipRepository repository)
        {
            return await RunChangeRelationships(
                repository.RemoveFromSubscribers,
                input,
                null,
                "REMOVE_FROM_SUBSCRIBERS_FAILED");            
        }

        public async Task<RelationshipPayload> AddToBlacklist(
            RelationshipInput input, [Service] IRelationshipRepository repository)
        {
            return await RunChangeRelationshipsWithNodeMerge(
                repository.AddToBlacklist,
                input,
                RelationType.HasBlocked,
                "ADD_TO_BLACKLIST_FAILED");            
        }
        
        public async Task<RelationshipPayload> RemoveFromBlacklist(
            RelationshipInput input, [Service] IRelationshipRepository repository)
        {
            return await RunChangeRelationships(
                repository.RemoveFromBlacklist,
                input,
                null,
                "REMOVE_FROM_BLACKLIST_FAILED");            
        }
    }
}
