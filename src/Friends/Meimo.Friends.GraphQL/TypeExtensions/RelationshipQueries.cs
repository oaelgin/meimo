using System.Collections.Generic;
using System.Threading.Tasks;
using HotChocolate;
using HotChocolate.Types;
using Meimo.Friends.Core;
using Meimo.Friends.GraphQL.Models;

namespace Meimo.Friends.GraphQL.TypeExtensions
{
    [ExtendObjectType(Constants.QueryTypeName)]
    public class RelationshipQueries
    {
        public async Task<IEnumerable<Relationship>> GetRelationships(
            RelationshipInput input, [Service] IRelationshipRepository repository)
        {
            return await repository.GetRelationships(input.FirstPersonId, input.SecondPersonId);
        }

        public async Task<bool> GetIsBlockedBy(
            RelationshipInput input, [Service] IRelationshipRepository repository)
        {
            return await repository.GetIsBlockedBy(input.FirstPersonId, input.SecondPersonId);
        }
    }
}
