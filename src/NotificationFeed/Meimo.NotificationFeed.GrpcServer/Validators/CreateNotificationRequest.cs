﻿using System;

using Grpc.Core;

namespace Meimo.NotificationFeed.GrpcServer
{
    public sealed partial class CreateNotificationRequest
    {
        public void Validate()
        {
            if (!Guid.TryParse(PersonId, out _))
            {
                throw new RpcException(new Status(StatusCode.InvalidArgument, "Person Id must be a GUID"));
            }

            if (!Guid.TryParse(EventId, out _))
            {
                throw new RpcException(new Status(StatusCode.InvalidArgument, "Event Id must be a GUID"));
            }

            if (string.IsNullOrWhiteSpace(Message))
            {
                throw new RpcException(new Status(StatusCode.InvalidArgument,
                    "Message cannot be null or white space"));
            }
        }
    }
}
