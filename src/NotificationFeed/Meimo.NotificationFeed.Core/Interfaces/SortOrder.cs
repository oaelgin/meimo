﻿namespace Meimo.NotificationFeed.Core
{
    public enum SortOrder
    {
        Ascending = 1,
        Descending = -1
    }
}
