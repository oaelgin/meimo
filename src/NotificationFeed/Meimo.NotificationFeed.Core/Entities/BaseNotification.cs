﻿using System;

using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;


namespace Meimo.NotificationFeed.Core
{
    public class BaseNotification
    {
        [BsonId]
        [BsonIgnoreIfDefault]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonRequired]
        public Guid PersonId { get; set; }

        [BsonRequired]
        public Guid EventId { get; set; }

        [BsonRequired]
        public int CreateRetries { get; set; }

        [BsonRequired]
        public bool HasBeenRead { get; set; }

        [BsonRequired]
        public bool PushSent { get; set; }

        [BsonRequired]
        public bool EmailSent { get; set; }
    }
}
