﻿using MongoDB.Bson.Serialization.Attributes;

namespace Meimo.NotificationFeed.Core
{

    public class Notification : BaseNotification
    {
        [BsonRequired]
        public string Message { get; set; }
    }
}
