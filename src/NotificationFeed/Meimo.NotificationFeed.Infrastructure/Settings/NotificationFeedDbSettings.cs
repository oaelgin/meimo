﻿namespace Meimo.NotificationFeed.Infrastructure
{
    public class NotificationFeedDbSettings : INotificationFeedDbSettings
    {
        public string CollectionName { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }
}
